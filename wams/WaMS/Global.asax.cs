﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WaMS.Models;

namespace WaMS
{
    public class MvcApplication : System.Web.HttpApplication
    {


        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            
            //recreating database based on model changes
            Database.SetInitializer<Models.WaMSContext>(new DropCreateDatabaseIfModelChanges<Models.WaMSContext>());




        }
        private WaMSContext db = new WaMSContext();

        public void dataInitializer()
        {
           //initializing the five bands that people can be fans of
            int bandCounter = 0;

            foreach (var item in db.Bands)
            {
                if (db.Bands.Find(item.BandID) != null)
                {
                    bandCounter++;
                }
            }
            if (bandCounter == 0)
            {
                Band band1 = new Band();
                band1.BandName = "The Beatles";
                Band band2 = new Band();
                band2.BandName = "The Rolling Stones";
                Band band3 = new Band();
                band3.BandName = "Franz Ferdinand";
                Band band4 = new Band();
                band4.BandName = "The Ramones";
                Band band5 = new Band();
                band5.BandName = "Pink Floyd";

                db.Bands.Add(band1);
                db.Bands.Add(band2);
                db.Bands.Add(band3);
                db.Bands.Add(band4);
                db.Bands.Add(band5);
                db.SaveChanges();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WaMS.Models;

namespace WaMS.Controllers
{
    public class BandsController : Controller
    {
        private WaMSContext db = new WaMSContext();

        // GET: Bands
        public ActionResult Index()
        {
            updatefancounter();                                                         //ensures number of fans is up to date when the index is viewed
            return View(db.Bands.ToList());
        }

        // GET: Bands/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Band band = db.Bands.Find(id);
            if (band == null)
            {
                return HttpNotFound();
            }

            getfancounter(band, id);                                                    //updates number of fans for current band
            ViewFans(id);                                                               //retrieves list of fans for current band
            return View(band);
        }

        //displays one particular fan and their information
        public ActionResult DetailsPerson (int? id)
        {
            var PeopleController = DependencyResolver.Current.GetService<PeopleController>();
            var result = PeopleController.viewfans(id);
            Person hugefan = db.People.Find(id);
            return View(hugefan);
        }



        //calls getfancounter with every band in order to update all fan numbers
        public int updatefancounter ()
        {
            foreach( var item in db.Bands)
            {
                getfancounter(item, item.BandID);
            }
            return 0;
        }

        //updates the number of fands for the current band being sent in
        public Band getfancounter(Band band, int? id)
        {
            int fancount = 0;
            switch (band.BandName)
            {
                case "The Beatles":
                {
                    foreach(var item in db.People)
                        {                            
                            if (item.BandFan1 == true)
                            {
                                fancount++;
                            }
                        }
                    band.fancounter = fancount;
                        break;
                }
                case "The Rolling Stones":
                    {
                        foreach (var item in db.People)
                        {
                            if (item.BandFan2 == true)
                            {
                                fancount++;
                            }
                        }
                        band.fancounter = fancount;
                        break;
                    }
                case "Franz Ferdinand":
                    {
                        foreach (var item in db.People)
                        {
                            if (item.BandFan3 == true)
                            {
                                fancount++;
                            }
                        }
                        band.fancounter = fancount;
                        break;
                    }
                case "The Ramones":
                    {
                        foreach (var item in db.People)
                        {
                            if (item.BandFan4 == true)
                            {
                                fancount++;
                            }
                        }
                        band.fancounter = fancount;
                        break;
                    }
                case "Pink Floyd":
                    {
                        foreach (var item in db.People)
                        {
                            if (item.BandFan5 == true)
                            {
                                fancount++;
                            }
                        }
                        band.fancounter = fancount;
                        break;
                    }

                default:
                    break;
            }

            return band;
        }


        //updates and lists all fans for the current band id
        public ActionResult ViewFans(int? id)
        {
            Band band = db.Bands.Find(id);
            List<Person> fanlist = new List<Person>();


            switch (band.BandName)
            {
                case "The Beatles":
                    {
                        foreach (var item in db.People)
                        {
                            if (item.BandFan1 == true)
                            {
                                fanlist.Add(item);
                            }
                        }
                        break;
                    }
                case "The Rolling Stones":
                    {
                        foreach (var item in db.People)
                        {
                            if (item.BandFan2 == true)
                            {
                                fanlist.Add(item);
                            }
                        }
                        break;
                    }
                case "Franz Ferdinand":
                    {
                        foreach (var item in db.People)
                        {
                            if (item.BandFan3 == true)
                            {
                                fanlist.Add(item);
                            }
                        }
                        break;
                    }
                case "The Ramones":
                    {
                        foreach (var item in db.People)
                        {
                            if (item.BandFan4 == true)
                            {
                                fanlist.Add(item);
                            }
                        }
                        break;
                    }
                case "Pink Floyd":
                    {
                        foreach (var item in db.People)
                        {
                            if (item.BandFan5 == true)
                            {
                                fanlist.Add(item);
                            }
                        }
                        break;
                    }

                default:
                    break;
            }

            band.Fans = fanlist;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        /*
            Create, edit, and delete are not currently being used in this implementation of the project. They are being left in for future iterations 
             */

        // GET: Bands/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Bands/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BandID,BandName")] Band band)
        {
            if (ModelState.IsValid)
            {
                db.Bands.Add(band);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(band);
        }

        // GET: Bands/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Band band = db.Bands.Find(id);
            if (band == null)
            {
                return HttpNotFound();
            }
            return View(band);
        }

        // POST: Bands/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BandID,BandName")] Band band)
        {
            if (ModelState.IsValid)
            {
                db.Entry(band).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(band);
        }

        // GET: Bands/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Band band = db.Bands.Find(id);
            getfancounter(band, id);
            if (band == null)
            {
                return HttpNotFound();
            }
            return View(band);
        }

        // POST: Bands/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Band band = db.Bands.Find(id);
            db.Bands.Remove(band);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

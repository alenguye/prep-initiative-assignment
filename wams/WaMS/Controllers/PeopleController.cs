﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WaMS.Models;

namespace WaMS.Controllers
{
    public class PeopleController : Controller
    {
        private WaMSContext db = new WaMSContext();

        // GET: People
        public ActionResult Index()
        {
            return View(db.People.ToList());
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }
        public ActionResult viewfans(int? id)
        {
            return RedirectToAction("Index");
        }
        public ActionResult ExtendedIndex()
        {
            return View(db.People.ToList());
        }

        //initilizing test data, if data already exists does nothing
        public ActionResult Initialize()
        {
            int personCounter = 0;

            foreach (var item in db.People)
            {
               if (db.People.Find(item.PersonID) != null)
                {
                    personCounter++;
                }
            }
            if (personCounter == 0)
            {
                Person alex = new Person();                                     //creating new person and their attributes
                alex.FirstName = "Alex";
                alex.LastName = "Nguyen";
                alex.Email = "alenguye@gmail.com";
                alex.BandFan1 = true;
                alex.BandFan2 = true;
                alex.BandFan3 = true;
                alex.BandFan4 = true;
                alex.BandFan5 = true;

                Person Sydney = new Person();
                Sydney.FirstName = "Sydney";
                Sydney.LastName = "Skopos";
                Sydney.Email = "sskopos@gmail.com";
                Sydney.BandFan1 = true;
                Sydney.BandFan2 = true;
                Sydney.BandFan3 = false;
                Sydney.BandFan4 = false;
                Sydney.BandFan5 = true;

                Person Russell = new Person();
                Russell.FirstName = "Russell";
                Russell.LastName = "Wang";
                Russell.Email = "Russell@gmail.com";
                Russell.BandFan1 = false;
                Russell.BandFan2 = false;
                Russell.BandFan3 = false;
                Russell.BandFan4 = true;
                Russell.BandFan5 = true;

                db.People.Add(alex);                                            //saving people to database
                db.People.Add(Sydney);
                db.People.Add(Russell);
                db.SaveChanges();


            }
 
            return RedirectToAction("Index");

        }

        // GET: People/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PersonID,FirstName,LastName,Email,BandFan1,BandFan2,BandFan3,BandFan4,BandFan5")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(person);
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PersonID,FirstName,LastName,Email,BandFan1,BandFan2,BandFan3,BandFan4,BandFan5")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

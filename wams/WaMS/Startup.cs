﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WaMS.Startup))]
namespace WaMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

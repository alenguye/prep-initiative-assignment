﻿using System.ComponentModel.DataAnnotations;

namespace WaMS.Models
{
    public class Person
    {
        public int PersonID { get; set; }

        [Required()]
        [Display(Name ="First Name")]
        [StringLength(50, ErrorMessage = "The name you have entered is too long. Please use a name with 50 chracters or less.")]
        public string FirstName { get; set; }

        [Required()]
        [Display(Name = "Last Name")]
        [StringLength(50, ErrorMessage ="The name you have entered is too long. Please use a name with 50 chracters or less.")]
        public string LastName { get; set; }

        
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Please re-enter a valid email address.")]
        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }


        [Display(Name = "The Beatles" )]
        public bool BandFan1 { get; set; }

        [Display(Name = "The Rolling Stones")]
        public bool BandFan2 { get; set; }

        [Display(Name = "Franz Ferdinand")]
        public bool BandFan3 { get; set; }

        [Display(Name = "The Ramones")]
        public bool BandFan4 { get; set; }

        [Display(Name = "Pink Floyd")]
        public bool BandFan5 { get; set; }

    }
}
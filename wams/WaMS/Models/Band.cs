﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WaMS.Models
{
    public class Band
    {
        public int BandID { get; set; }

        [Display(Name= "Band Name")]
        public string BandName { get; set; }
      
        public Person Fan { get; set; }

        [Display(Name = "Number of Fans")]
        public int fancounter { get; set; }


        public List<Person> Fans { get; set; }

        public Person PersonID { get; set; }
    }
}